import cx_Oracle
import string
import random

def random_string(length):
    return ''.join(random.choice(string.ascii_letters) for m in xrange(length))

# connect to DB
dsn_tns = cx_Oracle.makedsn('127.0.0.1', '1521', 'orcl')
con = cx_Oracle.connect('sys', 'Qwerty123]', dsn_tns, mode=cx_Oracle.SYSDBA)

print con.version

# insert data
rows = []		 
for x in range(0, 100):
	rows.append ([
		random_string(4),
		random_string(6),
		random_string(1),
		random_string(10),
		random_string(6),
		random_string(2),
		random_string(16),
		random_string(16),
		random_string(20),
		random_string(30),
		random_string(20),
		random_string(22),
		random_string(20),
		random_string(8),
		random_string(6),
		random_string(6),
		random.randint(1,101),
		random.randint(1,101),
		random_string(2),
		random_string(2),
		random_string(4),
		random_string(20),
		random_string(20),
		random_string(16),
		random_string(6),
		random_string(14),
		random_string(14),
		random_string(2)
	])

cur = con.cursor()
cur.bindarraysize = 100
cur.executemany("insert into CDR (FILENO, RECSEQNO, RECTYPE, TAC, CALLID, CALLINGSUBSTYPE, IMSI, IMEI, CALLINGPARTY, CALLEDPARTY, MOBILEROAMINGNO, ORIGINALCALLEDNO, REDIRECTINGNO, DATESTART, TIMESTART, TIMESTOP, UNCHARGEDDURATION, DURATION,  DISCONNECTINGPARTY, CHARGEDPARTY, CHARGINGCASE, OUTGOINGROUTE, NCOMINGROUTE, LOCATIONNO, TIMETCSEIZURE, CELLIDFIRST, CELLIDLAST, TELESERVICE) values (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28)", rows)
con.commit()
# close connection
con.close()