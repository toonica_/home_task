# Home Task

## Goal
Migrate and store CDR table from current relational DB into Hive. Make it a daily job.

## Užduoties eiga 

  - Hadoop aplinkos paruošimas;
  - Java instaliavimas;
  - Apache Derby instaliavimas;
  - Hive instaliavimas;
  - Oracle Database instaliavimas;
  - CDR lentelės kūrimas pagal duotą struktūrą;
  - dummy.py scripto kūrimas;
  - import.bat scripto kūrimas. 